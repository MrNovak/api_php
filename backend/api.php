<?php

include_once 'medicalAppoinmet.php';
include_once 'roles.php';

class Api{

    private $model;
    private $rol;

    public function __construct()
    {
        $this->model = new MedicalAppointment;
        $this->rol = new Roles;

    }

    public function getAllMedicalAppoinmetToday()
    {
        $medicalAppointmets = array();
        $medicalAppointmets["data"] = array();

        $response = $this->model->getTodayAppointments();

        if(!$response->rowCount()) {
            return json_encode(array('message' => 'Today you dont have pending any medical appointmet'));
        }

        foreach ($response as $value) {
            $item = array(
                'id' => $value['id'],
                'description' => $value['description'],
                'status' => $value['status'],
                'created_at' => $value['created_at']
            );

            array_push($medicalAppointmets['data'], $item);
        }

        return json_encode($medicalAppointmets);
    }

    public function changeStatusMedicalAppointment($appointment, $status)
    {
        $response = $this->model->changeStatus($appointment, $status);

        if(!$response)
            return false;

        return true;
    }

    public function createNewMedicalAppointment($description)
    {
        $response = $this->model->create($description);

        if(!$response)
            return false;
        
        return true;
    }

    public function consultRol($user)
    {
        $roles = array();
        $roles["data"] = array();

        $response = $this->rol->consultRol($user);

        if(!$response->rowCount()) {
            return json_encode(array('message' => 'Today you dont have pending any medical appointmet'));
        }

        foreach ($response as $value) {
            $item = array(
                'name' => $value['name'],
            );

            array_push($roles['data'], $item);
        }

        return json_encode($roles);
    }
}