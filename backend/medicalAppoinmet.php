<?php

include_once 'db/db.php';

class MedicalAppointment extends DB {

    public function getTodayAppointments()
    {
        return $this->connect()->query("select * from medical_appointments where date(created_at) = current_date()");
    }

    public function changeStatus($appointment, $status)
    {
        return $this->connect()->query("UPDATE medical_appointments SET status = '{$status}' where id = {$appointment}");
    }

    public function create($description)
    {
        return $this->connect()->query("INSERT INTO medical_appointments (description) values ('{$description}')");
    }
}