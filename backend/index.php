<?php

include_once 'api.php';

$api = new Api();

/**
 * ? This is for get the today medical appointments
 */
// echo($api->getAllMedicalAppoinmetToday());

/**
 * ? This is for change the status of the appointment
 */
// echo($api->changeStatusMedicalAppointment(1, "confirmed"));

/**
 * ? This is for create a new appointment
 */
// echo($api->createNewMedicalAppointment("This is a new medical appointment"));

/**
 * ? This is for consult Roles
 */
// echo($api->consultRol(4));