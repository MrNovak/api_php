<?php

include_once 'db/db.php';

class Roles extends DB {

    public function consultRol($user)
    {
        return $this->connect()->query("select roles.name_role as name from model_has_roles join roles on roles.id = model_has_roles.rol_id where model_has_roles.model_id = {$user}");
    }
}